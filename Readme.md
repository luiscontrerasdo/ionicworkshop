![Taller IONIC](/logos/tallerionic.png)

## IONIC WorkShop

Este workshop se imparte bajo solicitud, y está orientado sobre un entorno Microsoft Windows, en caso de utilizar Linux, el proceso de instalación y configuración de las herramientas no están contenidas en este material, por lo que el participante deberá hacerlo por su propia cuenta.

Aqui estarán los detalles para el WorkShop de IONIC.

Debe tener instalado:

Descargar *NodeJS:* https://nodejs.org/en/download/

Descargar *Android Studio:* https://developer.android.com/studio/#downloads

Descargar *JDK 8:* http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Descargar *Genymotion:* https://www.genymotion.com/fun-zone/

Nota: Es importante tener un usuario creado en la página de genymotion para poder descargarlo y también poder descargar las versiones de móviles que vamos a utilizar.

Descargar *Oracle VirtualBox:* http://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html

Nota: Genymotion requiere VirtualBox para que funcione.

Descargar *VS Code:* https://code.visualstudio.com/download

Descargar *Git para Windows:* https://git-scm.com/download/win

Aquí se encuentran todas las herramientas mencionadas anteriormente y las que se utilizaran durante el Workshop: https://drive.google.com/drive/folders/1sY7MSEw4-uIR_7J2ONCeBBZ8CQwxa-kD?usp=sharing

En la carpeta de Google Drive donde están las herramientas que utilizaremos, también he agregado los instaladores de:

*Google Chrome
*Firefox
*El editor Atom
*El editor Sublime Text
*El editor Bracket

# Referencias:

HTML5: https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5

CSS3: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS3

Javascript: https://developer.mozilla.org/bm/docs/Web/JavaScript

IONIC: https://ionicframework.com/docs/

Git: https://git-scm.com/doc

AngularJS: https://docs.angularjs.org/guide

NodeJS: https://nodejs.org/en/docs/

NPM: https://docs.npmjs.com/getting-started/what-is-npm




# Notas adicionales:

0. Los siguientes pasos deben estar listos previo a tomar el Workshop

1. Aquí se encuentran todas las herramientas mencionadas anteriormente y las que se utilizaran durante el Workshop: https://drive.google.com/drive/folders/1sY7MSEw4-uIR_7J2ONCeBBZ8CQwxa-kD?usp=sharing

2. Es importante tener todas herramientas instaladas:

* NodeJS 

* Android Studio 

* JDK 8 

* Genymotion 

* VS Code 

* Git 

* VirtualBox


3. Para el caso de Android Studio es importante que en el SDK Manager estén instalados y actualizados los mas recientes, este proceso toma tiempo dependiendo de la velocidad de internet. ES DE VITAL IMPORTANCIA QUE LO HAGA, ya que el Genymotion pudiera no funcionar para el proposito de nuestros ejercicios.

Ver los siguientes pasos que se deben realizar después de instalar Android Studio:

## Paso 1:

Cuando ejecutamos Android Studio, nos aparecerá una ventana similar a la siguiente:

![Paso 1](/android-studios-pasos/paso1.PNG)

## Paso 2:

Nos vamos a donde dice Configure, y elegímos la opción SDK Manager.

![Paso 2](/android-studios-pasos/paso2.png)



## Paso 3:

Nos saldrá una pantalla similar a la siguiente, del lado izquierdo hay varias opciones, por lo que debmos elegír Android SDK, y del lado derecho nos saldra un listado de las diferentes versiones de SDK, si se fija en la columna que dice API Level, es recomendable tener de la 20 hasta la última.

![Paso 3](/android-studios-pasos/paso3.PNG)

Después de haber cotejado las versiones necesarias, hacemos click sobre el botón Apply.

## Paso 4:

Nos saldrá una ventana similar a esta, con las versiones que hemos elegído, por lo que hacemos click sobre el botón de OK.

![Paso 4](/android-studios-pasos/paso4.PNG)


## Paso 5:

Empezará a ver una ventana similar a esta:

![Paso 5](/android-studios-pasos/paso5.png)



## Paso 6:

Y luego otra similar a esta:

![Paso 6](/android-studios-pasos/paso6.PNG)



## Paso 7:

Como ya ha concluído, presionamos el botón de Finish.

![Paso 7](/android-studios-pasos/paso7.PNG)



## Paso 8:

Y esta pantalla presionamos el botón de Ok, y hacemos hemos concluído con el SDK Manager.

![Paso 8](/android-studios-pasos/paso8.PNG)


# Configuración inicial de Git

Es importante que usted cuente con una cuenta en Github o GitLab, si no la tiene, debe crearla previamente para así poder completar los ejercicios mas adelante. 

Después de haber instalado git para Windows en su equipo, abra una línea de comandos y ejecute las siguientes instrucciones en orden:

**git config --global user.name "Tu nombre"**

**git config --global user.email "Tu email"**




