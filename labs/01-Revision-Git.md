![Taller IONIC](/logos/tallerionic.png)

## REVISANDO GIT

Recuerde que debe tener una cuenta de GitHub o GitLab creada.

Si desea saber la versión de Git que esta corriendo en su sistema operativo, solamente debe ejecutar: *git --version*

0.	Descargue Git desde aqui https://git-scm.com/download/win

1.	Proceda a su instalación

2.	Después de haberlo instalado, en la carpeta My Documents (Mis Documentos) cree una carpeta llamada Taller-IONIC

3.	Ahora proceda a abrir una línea de comando, y ejecute desde ahi, lo siguiente:

    **git config --global user.name "Tu nombre"**

    **git config --global user.email "Tu email"**

NOTA: Para realizar este paso debe haber previamente creado su repositorio en GitHub o GitLab.

4. Desde la misma línea de comandos, entre al directorio Taller-IONIC y ejecute lo siguiente de acuerdo a su cuenta si es de GitHub o GitLab:

Si tienes una cuenta de GitHub, seguir los siguientes pasos:
1.	echo “# mirespositorio” >> README.md
2.	git init
3.	git add README.md
4.	git commit –m “mi primer mensaje”
5.	git remote add origin https://github.com/luiscontrerasdo/mirepositorio.git
6.	git push –u origin master

Si tienes una cuenta de GitLab, seguir los siguientes pasos:
1.	echo “# mirespositorio” >> README.md
2.	git init
3.	git add README.md
4.	git commit –m “mi primer mensaje”
5.	git remote add origin https://gitlab.com/luiscontrerasdo/mirepositorio.git
6.	git push –u origin master



