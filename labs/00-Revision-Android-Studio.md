![Taller IONIC](/logos/tallerionic.png)

## REVISANDO ANDROID STUDIO

Para el caso de Android Studio es importante que en el SDK Manager estén instalados y actualizados los mas recientes, este proceso toma tiempo dependiendo de la velocidad de internet. ES DE VITAL IMPORTANCIA QUE LO HAGA, ya que el Genymotion pudiera no funcionar para el proposito de nuestros ejercicios.

Ver los siguientes pasos que se deben realizar después de instalar Android Studio:

## Paso 1:

Cuando ejecutamos Android Studio, nos aparecerá una ventana similar a la siguiente:

![Paso 1](/android-studios-pasos/paso1.PNG)

## Paso 2:

Nos vamos a donde dice Configure, y elegímos la opción SDK Manager.

![Paso 2](/android-studios-pasos/paso2.png)



## Paso 3:

Nos saldrá una pantalla similar a la siguiente, del lado izquierdo hay varias opciones, por lo que debmos elegír Android SDK, y del lado derecho nos saldra un listado de las diferentes versiones de SDK, si se fija en la columna que dice API Level, es recomendable tener de la 20 hasta la última.

![Paso 3](/android-studios-pasos/paso3.PNG)

Después de haber cotejado las versiones necesarias, hacemos click sobre el botón Apply.

## Paso 4:

Nos saldrá una ventana similar a esta, con las versiones que hemos elegído, por lo que hacemos click sobre el botón de OK.

![Paso 4](/android-studios-pasos/paso4.PNG)


## Paso 5:

Empezará a ver una ventana similar a esta:

![Paso 5](/android-studios-pasos/paso5.png)



## Paso 6:

Y luego otra similar a esta:

![Paso 6](/android-studios-pasos/paso6.PNG)



## Paso 7:

Como ya ha concluído, presionamos el botón de Finish.

![Paso 7](/android-studios-pasos/paso7.PNG)



## Paso 8:

Y esta pantalla presionamos el botón de Ok, y hacemos hemos concluído con el SDK Manager.

![Paso 8](/android-studios-pasos/paso8.PNG)


