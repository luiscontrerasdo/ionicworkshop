![Taller IONIC](/logos/tallerionic.png)

## REVISANDO JAVA

Abra una ventana de command prompt para verificar si tenemos Java instalador correctamente, solamente ejecutamos *java -version* y debemos tener un resultado similar a este:

**java version "1.8.0_151"**

**Java(TM) SE Runtime Environment (build 1.8.0_151-b12)**

**Java HotSpot(TM) 64-Bit Server VM (build 25.151-b12, mixed mode)**

